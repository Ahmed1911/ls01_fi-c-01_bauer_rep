package proj_Test01;

public class Main {

	public static void main(String[] args) {
		ausgabe1();
		ausgabe3();

	}
	
	static void ausgabe0() {
		int alter = 19;
		String name = "Ahmed";
		System.out.println("Hallo mein Name ist " + name + " und ich bin " + alter + " Jahre alt.");
	}
	
	static void ausgabe1() {
		System.out.println("           **          ");
		System.out.println("        *      *       ");
		System.out.println("        *      *       ");
		System.out.println("           **          ");
	}
	
	static void ausgabe2() {
		//TODO
	}
	
	
	static void ausgabe3() {
		double c1 = -28.8889;
		double c2 = -23.3333;
		double c3 = -17.7778;
		double c4 = -6.6667;
		double c5 = -1.1111;
		
		System.out.println("Fahrenheit    |  Celsius");
		System.out.println("------------------------");
		System.out.print("-20");
		System.out.printf( "%12s", "|");
		System.out.printf( "%10s\n", runden(c1));
		System.out.print("-10");
		System.out.printf( "%12s", "|");
		System.out.printf( "%10s\n", runden(c2));
		System.out.print("0  ");
		System.out.printf( "%12s", "|");
		System.out.printf( "%10s\n", runden(c3));
		System.out.print("20 ");
		System.out.printf( "%12s", "|");
		System.out.printf( "%10s\n", runden(c4));
		System.out.print("30 ");
		System.out.printf( "%12s", "|");
		System.out.printf( "%10s\n", runden(c5));
	}
	
	static double runden(double input) {
		return Math.round(input * 100.0) / 100.0;
	}

}
