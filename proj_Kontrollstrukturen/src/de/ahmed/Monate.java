package de.ahmed;

import java.util.Scanner;

public class Monate {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		
		System.out.println("Geben Sie eine Zahl zwischen 1 und 12 ein:");
		int input = sc.nextInt();
		
		if(input < 1 || input > 12) {
			System.out.println("! Achtung ! Nur zahlen zwischen 1 und 12 erlaubt.");
		} else {
			String month = "";
			
			switch (input) {
			case 1: month = "Januar"; break;
			case 2: month = "Februar"; break;
			case 3: month = "M�rz"; break;
			case 4: month = "April"; break;
			case 5: month = "Mai"; break;
			case 6: month = "Juni"; break;
			case 7: month = "Juli"; break;
			case 8: month = "August"; break;
			case 9: month = "September"; break;
			case 10: month = "Oktober"; break;
			case 11: month = "November"; break;
			case 12:month = "Dezember"; break;
			}
			
			System.out.println(month);
		}
	}

}
