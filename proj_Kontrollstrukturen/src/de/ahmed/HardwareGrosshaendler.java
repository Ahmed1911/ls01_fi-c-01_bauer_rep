package de.ahmed;

import java.util.Scanner;

public class HardwareGrosshaendler {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double preisMaus = 19.99, lieferPauschale = 10.0;
		
		System.out.print("Wie viele M�use wurden Bestellt?");
		int input = sc.nextInt();
		
		double rechnungsBetrag = input*preisMaus;
		double versandPreis = 10.00;
		
		System.out.println("------------------------");
		System.out.println("Zwischensumme: " + betragRunden(rechnungsBetrag) + "�");
		
		
		if(input < 10) {
			rechnungsBetrag += versandPreis;
			System.out.println("Versand: " + betragRunden(versandPreis) + "�");
		} else {
			System.out.println("Versand: 0�");
		}
		
		rechnungsBetrag = rechnungsBetrag*1.19;
		System.out.println("Steuern (19%): " + betragRunden(rechnungsBetrag*0.19) + "�");
		
		System.out.println("------------------------");
		System.out.println("Gesamtbetrag: " + betragRunden(rechnungsBetrag) + "�");
	}

	public static double betragRunden(double pBetrag) {
		return Math.round(pBetrag * 100.0) / 100.0;
		
	}
	
}
