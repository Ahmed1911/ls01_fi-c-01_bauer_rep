package de.ahmed;

import java.util.Scanner;

public class Steuersatz {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		float USt = 0.19f, USt_ermaesigt = 0.16f;
		
		System.out.print("Geben Sie den Nettobetrag ein: ");
		double input = sc.nextDouble();
		
		System.out.println("M�chten Sie den ermaeigten Steursatz von " + USt_ermaesigt*100 + "% anwenden? (j) ja, (n) nein.");
		
		float result;
		if(sc.next().equalsIgnoreCase("j")) {
			result = (float)input*(1+USt_ermaesigt);
		} else {
			result = (float)input*(1+USt);
		}

		System.out.print("Der Bruttobetrag lautet: ");
		System.out.printf("%.02f", result);
		
		sc.close();
	}

}
