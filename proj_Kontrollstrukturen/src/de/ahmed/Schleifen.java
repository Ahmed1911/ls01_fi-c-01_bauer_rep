package de.ahmed;

import java.util.Scanner;

@SuppressWarnings("resource")
public class Schleifen {

	public static void main(String[] args) {
		aufgabe9();
	}

	public static void aufgabe1() {
		System.out.println("Bitte n eingeben:");
		int n = new Scanner(System.in).nextInt();

		for (int i = 1; i <= n; i++) {
			System.out.println(i);
		}

		for (int i = n; i > 0; i--) {
			System.out.println(i);
		}
	}

	public static void aufgabe2() {
		System.out.println("Bitte n eingeben:");
		int n = new Scanner(System.in).nextInt();

		int ergA = 0, ergB = 0, ergC = 0;

		for (int i = 0; i <= n; i++) {
			ergA += i;
		}

		for (int i = 0; i <= n; i += 2) {
			ergB += i;
		}

		for (int i = 0; i < n; i += 2) {
			ergC += i + 1;
		}

		System.out.println("Die Summe für A beträgt: " + ergA);
		System.out.println("Die Summe für B beträgt: " + ergB);
		System.out.println("Die Summe für C beträgt: " + ergC);
	}

	public static void aufgabe3() {
		System.out.println("Folgende Zahlen sind durch 7, nicht durch 5 aber durch 4 teilbar: ");
		for (int i = 0; i < 200; i++) {
			if ((i % 7 == 0) && (i % 5 != 0) && (i % 4 == 0)) {
				System.out.print(i + " ");
			}
		}
	}

	public static void aufgabe4() {
		System.out.print("a) ");
		for (int i = 99; i > 0; i -= 3) {
			System.out.print(i + " ");
		}

		System.out.print("b) ");
		for (int i = 0; i < 100; i++) {
			System.out.print(i + " ");

		}
	}

	public static void aufgabe9() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Treppenhöhe: ");
		int h = sc.nextInt();
		System.out.println("Treppenbreite: ");
		int b = sc.nextInt();


		int cursorH = h-1;
		for (int i = 0; i < h; i++) {
			for (int j = cursorH; j > 0; j--) {
				for (int k = 0; k < b; k++) {
					System.out.print(" ");
				}
			}
			for(int x = h-cursorH; x > 0; x--) {
				for (int j = 0; j < b; j++) {
					System.out.print("*");
				}
			}

			System.out.println("");
			cursorH--;
		}

	}

}
