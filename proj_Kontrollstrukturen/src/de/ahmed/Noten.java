package de.ahmed;

import java.util.HashMap;
import java.util.Scanner;

public class Noten {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		HashMap<Integer, String> notenMap = new HashMap<>();
		notenMap.put(1, "Sehr gut");
		notenMap.put(2, "Gut");
		notenMap.put(3, "Befriedigend");
		notenMap.put(4, "Ausreichend");
		notenMap.put(5, "Mangelhaft");
		notenMap.put(6, "Ungenügend");
		
		
		System.out.println("Geben Sie die Note ein:");
		int input = sc.nextInt();
		
		if(input < 1 || input > 6) {
			System.out.println("Bitte geben Sie eine Note zwischen 1 und 6 ein.");
		} else {
			System.out.println(notenMap.get(input));
		}
		
		sc.close();
	}

}
