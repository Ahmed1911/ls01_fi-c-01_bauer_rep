package proj_Methoden;

import java.util.Scanner;

public class PCHaendler {

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		liesString();
		liesInt();
		liesDouble();
		berechneGesamtnettopreis(5, 100.0);
		// berechneGesamtnettopreis(10.5, 19);
	}

	public static String liesString() {
		System.out.println("Was m�chten Sie bestellen?");
		return sc.next();
	}

	public static int liesInt() {
		System.out.println("Geben Sie die Anzahl ein: ");
		return sc.nextInt();
	}

	public static double liesDouble() {
		System.out.println("Geben Sie den Nettopreis ein: ");
		return sc.nextDouble();
	}

	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		System.out.println("Geben Sie den Umsatzsteuersatz in Prozent ein: ");
		return anzahl * nettopreis;
	}

	public static double berechneGesamtnettopreis(double nettogesamtpreis, int mwst) {
		// System.out.print("Preis: ");
		return nettogesamtpreis + mwst / 100;
	}
}
