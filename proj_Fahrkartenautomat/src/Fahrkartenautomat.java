﻿import java.util.Scanner;

class Fahrkartenautomat {

	enum Einheit {
		Cent, Euro
	}

	public static void main(String[] args) {
		double zuZahlenderBetrag = erfassenFahrkartenbestellung();
		double eingezahlterGesamtbetrag = bezahlenFahrkarten(zuZahlenderBetrag);
		ausgebenFahrkarten();
		ausgebenRueckgeld(eingezahlterGesamtbetrag, zuZahlenderBetrag);
	}

	public static double erfassenFahrkartenbestellung() {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Zu zahlender Betrag (EURO): ");
		float zuZahlenderBetrag = tastatur.nextFloat();
		
		System.out.print("Anzahl der Tickets: ");
		float anzahlTickets = tastatur.nextInt();
		
		while(anzahlTickets < 1 || anzahlTickets > 10) {
			System.out.println("Fehler! Es können nur 1-10 Tickets bestellt werden.");

			System.out.print("Anzahl der Tickets: ");
			anzahlTickets = tastatur.nextInt();
		}

		return zuZahlenderBetrag * anzahlTickets;
	}

	public static float bezahlenFahrkarten(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
		float eingezahlterGesamtbetrag = 0.0f;

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: " + "%.2f", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.println(" Euro");
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneMuenze = tastatur.nextFloat();
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}

		return eingezahlterGesamtbetrag;
	}

	public static void ausgebenFahrkarten() {
		System.out.println("\nFahrschein wird ausgegeben");

		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}

	public static void ausgebenRueckgeld(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;

		if (rueckgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von " + "%.2f", (rueckgabebetrag));
			System.out.println(" Euro ");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, Einheit.Euro);
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgeben(1, Einheit.Euro);
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50, Einheit.Cent);
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20, Einheit.Cent);
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, Einheit.Cent);
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				muenzeAusgeben(5, Einheit.Cent);
				rueckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}

	public static void warte(int pMillisekunden) {
		try {
			Thread.sleep(pMillisekunden);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void muenzeAusgeben(int pBetrag, Einheit pEinheit) {
		System.out.println(pBetrag + " " + pEinheit);
	}

}